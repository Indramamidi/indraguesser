//
//  FirstViewController.swift
//  indraguesser
//
//  Created by student on 2/28/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

   
    @IBOutlet weak var InputTF: UITextField!
    
    @IBOutlet weak var ResultL: UILabel!
    
    @IBAction func AmiBTN(_ sender: Any) {
        let input = Int(InputTF.text!)
        if input == nil{
            errorMsg()
            
        }
        else if input! < 0 || input! > 10{
            Inputerror()
            
        }
            
        else{
            ResultL.text = Guesser.shared.amIRight(guess: input!)
            if ResultL.text == "Correct"{
                displayMessage()
            }
        }
    }
    
    
    @IBAction func ProblemBTN(_ sender: Any) {
        Guesser.shared.createNewProblem()
        InputTF.text = ""
    }
    
  
    func displayMessage(){
        let alert = UIAlertController(title: "Well done",
                                      message: "You got it in \(Guesser.shared.numAttempts) tries",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
        Guesser.shared.createNewProblem()
        InputTF.text = ""
    }
    
    
    func errorMsg(){
        
        let alert = UIAlertController(title: "Error",
                                      message: "Please Enter the vaild Number in TextField",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        Guesser.shared.createNewProblem()
        InputTF.text = ""
    }
    
    func Inputerror(){
        
        let alert = UIAlertController(title: "Error",
                                      message: " Please Enter the vaild Number In between 0 and 10",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        Guesser.shared.createNewProblem()
        InputTF.text = ""
    }

}



